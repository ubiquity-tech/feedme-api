<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable =['vendor_id','caption','price','media_url','media_type','sold_out','expired'];

    public function users() {
        return $this->belongsToMany('App\User');
    }
}
