<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
	{
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        $name = $request->input('name');
		$email = $request->input('email');
		$password = $request->input('password');

        $user = new User([
            'name' => $name,
            'email' => $email,
            'password' => md5($password)
        ]);

        if ($user->save()) {
            $response['data'] = [
                'user' => $user,
                'msg' =>'User created'
            ];
            return response()->json($response, 201);
        }
            $response['data'] = [
                'msg' => 'Any error',
            ];
            return response()->json($response, 404);

    }

	public function signin(Request $request)
	{
		$this->validate($request, [
			'email' => 'bail|required|email',
			'password' => 'bail|required'
		]);

		$email = $request->input('email');
		$password = $request->input('password');
		$user =[
			'name' => 'Name',
			'email' => $email,
			'password' => $password
		];

		$response = [
			'msg' => 'User signed in',
			'user' => $user
		];

		return response()->json($response,200);

	}

}
