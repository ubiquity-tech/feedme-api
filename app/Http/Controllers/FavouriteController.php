<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post_id = $request->input('post_id');
        $user_id = $request->input('user_id');
        $response['data'] =[
            'msg' => 'Post favourite successfully'
        ];
        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return "It works";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $favourite =[
            'post_id' => 'post_id',
            'user_id' => 'user_id'
        ];
         $response =[
             'msg' => 'Un-favourite Successfully '
         ];
        return response()->json($response, 200);
    }
}
