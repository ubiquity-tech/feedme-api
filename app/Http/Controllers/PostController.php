<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        // $this ->middleware('name');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
//        foreach ($posts as $post){
//
//        }
        $response['data'] = [
            'msg' => 'List of All Posts',
            'post' => $posts

        ];
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vendor_id = $request->input('vendor_id');
        $caption = $request->input('caption');
        $price = $request->input('price');
        $media_url = $request->input('media_url');
        $media_type = $request->input('media_type');
        $sold_out = $request->input('sold_out');
        $expired = $request->input('expired');

        $post = new Post([
            'vendor_id' => $vendor_id,
            'caption' => $caption,
            'price' => $price,
            'media_url' => $media_url,
            'media_type' => $media_type,
            'sold_out' => $sold_out,
            'expired' => $expired


        ]);
        if($post->save()){
//            $post->users()->attach($vendor_id);
            $response['data'] = [
                'post' => $post,
                'msg' => 'Post Created'

            ];
            return response()->json($response, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with('users')->where('id',$id)->firstOrFail();

        $response['data'] = [
            'msg' => 'Post by Id',
            'post' => $post
        ];
        return response()->json($response, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor_id = $request->input('vendor_id');
        $caption = $request->input('caption');
        $price = $request->input('price');
        $media_url = $request->input('media_url');
        $media_type = $request->input('media_type');
        $sold_out = $request->input('sold_out');
        $expired = $request->input('expired');
        $post = [
            'vendor_id' => $vendor_id,
            'caption' => $caption,
            'price' => $price,
            'media_url' => $media_url,
            'media_type' => $media_type,
            'sold_out' => $sold_out,
            'expired' => $expired
        ];
        $response['data'] = [
            'msg' => 'Post updated',
            'post' => $post
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response['data'] = [
            'msg' => 'Post deleted successfully'
        ];
        return response()->json($response, 200);
    }
}
